package kot

import kot.class_variable.inheritance.book.Book

fun main() {
//    val user = UserOverriderFun()
//
//    user.name = null
//    user.age = -15
//
//    println("Name: ${user.name} Age: ${user.age}")

//    val dog = Dog()
//
//    dog.nickname = "rick"
//    dog.age = 5
//    dog.weight = 8.2F
//
//    println("Nickname: ${dog.nickname} Age: ${dog.age} Weight: ${dog.weight}")


    val book = Book("Dog", 2001, 200)

    println(book.price)

    book.yearOfRelease = 300
}