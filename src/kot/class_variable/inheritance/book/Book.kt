package kot.class_variable.inheritance.book

class Book(val name: String, var yearOfRelease: Int? = null, var price: Int? = null)