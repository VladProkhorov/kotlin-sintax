package kot.class_variable.inheritance.book

class UserOverriderFun {

    var age: Int = 0
        set(value) {
            if (value >= 0){
                field = value
            }
        }

    var name: String? = ""
        get() {
            return if (field == null) {
                ""
            } else {
                field
            }
        }
}
