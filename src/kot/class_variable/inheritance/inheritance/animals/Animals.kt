package kot.class_variable.inheritance.inheritance.animals

import javafx.scene.text.FontWeight

open class Animals(val name: String, var weight: Float, val habitat: String) {

    open fun eat(){
        println("Есть")
    }
    open fun run(){
        println("Бежать")
    }
}