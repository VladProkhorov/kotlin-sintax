package kot.class_variable.inheritance.inheritance.animals

import kotlin.text.capitalize as capitalize1

class Dog {

    var nickname: String = ""
        get() = field.toLowerCase().capitalize1()

    var age = 0
        set(value) {
            if (value >= 0) {
                field = value
            }
        }
    var weight = 0f
        set(value) {
            if (value >= 0) {
                field = value
            }
        }
}