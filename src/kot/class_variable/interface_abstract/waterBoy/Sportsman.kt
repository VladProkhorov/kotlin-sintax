package kot.class_variable.interface_abstract.waterBoy

class Sportsman {
    fun invokeWaterBoy(waterBoy: WaterBoy){
        waterBoy.bringWater()
    }

   inline fun invokeWaterBoy(bringWater: () -> Unit){
        bringWater()
    }
}