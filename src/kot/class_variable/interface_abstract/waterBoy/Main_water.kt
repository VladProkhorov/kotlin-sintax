package kot.class_variable.interface_abstract.waterBoy

fun main(){
    val sportsman = Sportsman()
//    sportsman.invokeWaterBoy(object : WaterBoy{
//        override fun bringWater(){
//            println("Вода принесена")
//        }
//    })

    sportsman.invokeWaterBoy{ println("Вода принесена") }

//    если писать на Андроиде, то можно передавать интерфейс через лямбду
//    пример с обработкой кнопки:

//    view.setOnClickListner{
//        сюда пишем функцию
//    }
}