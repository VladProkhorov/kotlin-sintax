package kot.class_variable.interface_abstract.workers

fun main() {

    val workers = mutableListOf<Worker>()

    workers.add(Saller("Leo", 27))
    workers.add(Saller("Roy", 30))
    workers.add(Saller("Kaolin", 45))
    workers.add(Programmer("Den", 23, "Kotlin"))
    workers.add(Programmer("Nick", 27, "Java"))
    workers.add(Director("Fil", 50))

    val cleaners = workers.filter{it is Clean }.map { it as Clean }
    for (cleaner in cleaners){
        cleaner.clean()
    }
}