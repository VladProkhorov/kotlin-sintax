package kot.class_variable.interface_abstract.workers

fun main() {

    val workers = mutableListOf<Worker>()
    workers.add(Saller("Leo", 27))
    workers.add(Saller("Roy", 30))
    workers.add(Saller("Kaolin", 45))
    workers.add(Programmer("Den", 23, "Kotlin"))
    workers.add(Programmer("Nick", 27, "Java"))
    workers.add(Director("Fil", 50))
    for (worker in workers) {
        worker.work()
        worker as Clean
        if (worker is Programmer) {
            println(worker.language)
        }
        if (worker is Clean) {
            worker.clean()
        }
    }
}