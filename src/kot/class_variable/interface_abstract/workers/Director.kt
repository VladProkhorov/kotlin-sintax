package kot.class_variable.interface_abstract.workers

class Director(name: String, age: Int): Worker(name, age) {

    override fun work() {
        println("I control the work process")
    }
}