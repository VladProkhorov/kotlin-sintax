package kot.class_variable.interface_abstract.workers

 abstract class Worker(val name: String, var age: Int) {

    abstract fun work()
}