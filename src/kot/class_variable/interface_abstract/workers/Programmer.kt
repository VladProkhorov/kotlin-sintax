package kot.class_variable.interface_abstract.workers

class Programmer(name: String, age:Int, val language: String): Worker(name,age),
    Clean {

    override fun work(){
        println("I write the code on: $language")

    }

    override fun clean() {
        println("clean the computer")
    }
}