package kot.class_variable.interface_abstract.workers

class Saller( name: String, age: Int): Worker(name,age),
    Clean {
    override fun work() {
        println("Sale thing")
    }

    override fun clean() {
        println("Clean of the desk")
    }
}