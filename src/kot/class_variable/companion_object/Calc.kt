package kot.class_variable.companion_object

class Calc {
    companion object {
        const val PI = 3.14
        fun square(num: Int) = num * num
        fun lengthOfCycle(radius: Float) = 2 * PI * radius
    }

}