package kot.class_variable.companion_object.random_numb_days_boolean

enum class WeekDays {

    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}