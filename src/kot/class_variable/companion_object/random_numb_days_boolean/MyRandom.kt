package kot.class_variable.companion_object.random_numb_days_boolean

import kot.class_variable.companion_object.random_numb_days_boolean.WeekDays.*

class MyRandom {

    companion object {
        fun randomInt(from: Int, to: Int) = (Math.random() * to - from + 1).toInt() + from

        fun randomBoolean() = randomInt(0, 1) > 0

        fun randomDayOfWeek(): WeekDays {
            val index = randomInt(1, 7)
            return when (index) {
                1 -> MONDAY
                2 -> TUESDAY
                3 -> WEDNESDAY
                4 -> THURSDAY
                5 -> FRIDAY
                6 -> SATURDAY
                else -> SUNDAY
            }
        }
    }
}