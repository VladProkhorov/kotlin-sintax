package kot.class_variable.companion_object

import kot.class_variable.companion_object.random_numb_days_boolean.MyRandom

fun main() {

//    println(Calc.square(4))
//    println(Calc.lengthOfCycle(5f))

    println(MyRandom.randomInt(5,10))
    println(MyRandom.randomBoolean())
    println(MyRandom.randomDayOfWeek())
}