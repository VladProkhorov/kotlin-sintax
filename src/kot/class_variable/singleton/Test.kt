package kot.class_variable.singleton

class Test {
    fun insertTestData(string: String){
        Singleton.insertData(string)
    }
}