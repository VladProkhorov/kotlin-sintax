package kot.class_variable.dataClassAddress

class DataAddress(var nameCity: String, var street: String, var numberHouse: Int){

//    operator fun component1() = nameCity
//    operator fun component2() = street
//    operator fun component3() = numberHouse


    fun copy(
        nameCity: String = this.nameCity,
        street: String = this.street,
        numberHouse: Int = this.numberHouse
    ): DataAddress{
        return DataAddress(nameCity,street,numberHouse)
    }
}
