package kot.class_variable.dataClassAddress

fun main(){

    val address1 = DataAddress("New York","Manhattan",12 )
    val address2 = address1.copy()

//    val(nameCity,street,numberHouse) = address1
//    println(nameCity)
//    println(street)
//    println(numberHouse)

    println(address1)
    println(address2)
    println(address1.hashCode())
    println(address2.hashCode())
    println(address1 === address2)

}