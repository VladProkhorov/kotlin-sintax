package kot.class_variable.enum

import kot.MonthEnum.*
import kot.class_variable.enum.MonthEnum.*
import kot.class_variable.enum.SeasonEnum.*

fun main(){

    val month: MonthEnum = JANUARY

    val seasonAge = when (month){

        DECEMBER,JANUARY,FEBRUARY-> WINTER
        MARCH,APRIL,MAY-> SPRING
        JUNE,JULY,AUGUST -> SUMMER
        SEPTEMBER,OCTOBER,NOVEMBER -> AUTUMN
    }

    println(month.tempAverage)
    println(seasonAge)
}