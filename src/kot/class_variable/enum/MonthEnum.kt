package kot.class_variable.enum

enum class MonthEnum (val tempAverage: Int){

    JANUARY(0),
    FEBRUARY(2),
    MARCH(10),
    APRIL(15),
    MAY(24),
    JUNE(28),
    JULY(29),
    AUGUST(23),
    SEPTEMBER(17),
    OCTOBER(12),
    NOVEMBER(10),
    DECEMBER(0)
}