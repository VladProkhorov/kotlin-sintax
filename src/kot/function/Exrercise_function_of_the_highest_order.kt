package kot.function

fun main() {
    val result = modifyString("hello") { it.toUpperCase() }
    println(result)
}

fun modifyString(string: String, modify: (String) -> String): String {
    return modify(string)
}