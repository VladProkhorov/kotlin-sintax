package kot.function.ExtentionFun

fun main(){

    val age = 5
    println(age.isPositive())
}

fun Int.validAge() = this in 6..100

fun Int.isPositive() = validAge()