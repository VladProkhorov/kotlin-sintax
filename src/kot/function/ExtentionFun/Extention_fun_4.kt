package kot.function.ExtentionFun

fun main(){
    val obj = mutableMapOf<String, String>()
    myWith(obj){
        keys
        values

    }
}


inline fun<T , R> myWith(obj: T, operation: T.() -> R) : R {
    return obj.operation()
}