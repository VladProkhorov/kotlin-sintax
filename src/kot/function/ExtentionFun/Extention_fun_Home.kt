package kot.function.ExtentionFun

//здесь нужно вернуть простое число

fun main() {

    println(30.isPrime())

}

fun Int.isPrime(): Boolean {
    if (this <= 3) {
        return true
    }
    for (i in 2 until this){
        if( this % i == 0) return false
    }
    return true
}

