package kot.function

import java.awt.AWTEventMulticaster.add

var numbers: MutableList<Int>? = mutableListOf()

fun main() {
    numbers?.let {
        with(it) {
            for (i in 0 until 1000) {
                add((Math.random() * 1000).toInt())
            }
            val result =  filter {  it % 2 == 0 }.take(100)
            for (i in result){
                println(i)
            }
        }
    }

}