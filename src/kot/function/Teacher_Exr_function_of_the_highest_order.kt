package kot.function

fun main() {
    val list = (0..100).toList()
    collectionFun(list) {
        println(it.sum())
    }
}

inline fun collectionFun(list: List<Int>, operator: (List<Int>) -> Unit) {
    return operator(list)
}
