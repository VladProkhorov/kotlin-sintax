package kot.Exercise_3

fun main() {
    val time = 22
    val weatherIsGood = true
    val result = when {
        time in 9..21 && weatherIsGood -> println("Гулять")
        time in 9..21 && !weatherIsGood -> println("Читать книгу дома")
        time in 21..24 && weatherIsGood -> println("Читать книгу на улице")
        else -> println("Спать")
    }
}