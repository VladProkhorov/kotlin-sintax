package kot.Exercise_3

fun main(){

        val indexOfMonth = 3
    val season = when (indexOfMonth) {
        12, 1, 2 -> {
            "Зима"
        }
        in 3..5 -> {
            "Весна"
        }
        in 6..8 -> {
            "Лето"
        }
        in 9..11 -> {
            "Осень"
        }

        else -> {
            "Не найдено!"
        }
    }
    println(season)
}