package kot.Exercise_3

fun main(){

    val tempWater = 10
    val state = when {
        tempWater < 0 || tempWater > 30 || tempWater > 60 -> "Твердое"
        tempWater in 0..100 -> "Жидкое"
        else -> "Газообраз"

    }
    println(state)


}