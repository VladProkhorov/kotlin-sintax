package kot.Exercise_3

fun main() {
    val nameOfMonth = "Март"
    val season = when (nameOfMonth) {
        "Декабрь", "Январь", "Февраль" -> {
            "Зима"
        }

        "Март", "Апрель", "Май" -> {
            "Весна"
        }

        "Июнь", "Июль", "Август" -> {
            "Лето"
        }

        "Сентябрь", "Октябрь", "Ноябрь" -> {
            "Осень"
        }
        else -> {
            "Не найдено!"
        }
    }
    println(season)
    }
