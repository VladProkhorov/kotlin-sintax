package kot.equals

class Student(var name: String, var lastName: String, var id: String) {
    operator fun component1() = name
    operator fun component2() = lastName
    operator fun component3() = id
     fun copy(name: String = this.name,lastName: String = this.lastName,id: String = this.id): Student{
    return Student(name, lastName, id)
}

}