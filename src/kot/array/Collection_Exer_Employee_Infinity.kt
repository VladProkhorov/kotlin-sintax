package kot.array

fun main() {
//    val array = (0..100)
    val result = generateSequence(0){ it }
    val employees = result.map { "Сотрудник №${it + 1}" }
    val infinity = employees.drop(100)
    for (employee in infinity ){
        println(employee)
    }

}