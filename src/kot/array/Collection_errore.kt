package kot.array

fun main(){

//    val dataPair = mapOf(
//        Pair("filter1", listOf(1, 5, 6, -2, 10, 9)),
//        Pair("filter2", listOf(0, 4, 67678, 88, 2, 9)),
//        Pair("filter3", listOf(0, 50, 7, 2, 2, 74))
//    )
//
//    val average23 = dataPair.filter { it.value.all { it >= 0} }.flatMap { it.value }.average()
//    println(average23)


    val dataPairAny = mapOf(
        Pair("filter1", listOf(1, 5, 6, -2, 10, 9)),
        Pair("filter2", listOf(0, 4, 67678, 88, 2, 9)),
        Pair("filter3", listOf(0, 50, 7, 2, 2, 74))
    )

    val averageAny = dataPairAny.filterNot { it.value.any { it < 0} }.flatMap { it.value }.average()
    println(averageAny)
}