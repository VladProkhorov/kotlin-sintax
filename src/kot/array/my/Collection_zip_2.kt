package kot.array.my

fun main() {

    var names = mutableListOf("Алина", "Игорь", "Елена", "Сергей", "Максим")

    while (true) {
        val name = names.shuffled().first()
        val phone = 79_000_000_000 + (Math.random() * 1_000_000_000).toLong()
        println("Имя: $name Телефон: $phone")
    }
}