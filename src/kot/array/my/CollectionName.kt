package kot.array.my

fun main() {

    val name = listOf<String>(
        "Владимир", "Елена", "Максим",
        "Евгения", "Алексей", "Лилия"
    )

    val numbersName = (0..6).toList()

    name.mapIndexed ({ index, s -> println("Сотрудник $s, их № ${numbersName[index]}")})
    a({}, a = 3)
    b(3) {

    }
//    println(listOfFirstLetter.joinToString())
//    for (listOfFirstLetter){
//        println(listOfFirstLetter)
//    }
//    name.forEachIndexed { index, s ->  }


//    val numbers = (0..100).toList()
//    val doubledNumbers = numbers.map {it * 2}
//    for (i in doubledNumbers){
//        println(i)
//    }
}

fun a(lambda: () -> Unit, a: Int) {}
fun b(a: Int, lambda: () -> Unit) {}