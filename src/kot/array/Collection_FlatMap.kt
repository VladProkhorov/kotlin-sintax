package kot.array

fun main() {

//    val revenueByWeek = listOf(
//        listOf(9, 6, 4, 5, 0, 7),
//        listOf(9, 1, 4, 0, 2, 7),
//        listOf(9, 0, 1, 5, 2, 7),
//        listOf(0, 6, 4, 1, 2, 7)
//    )
//
//    val total = revenueByWeek.flatten()
//    val average = total.average()
//    println(average)
//
//    val dataMapOf = mapOf(
//        "filter1" to listOf(0, 5, 6, 2, 2, 9),
//        "filter2" to listOf(4, 0, 6, 1, 3, 89),
//        "filter3" to listOf(4, 5, 0, 2, 3, 8)
//    )
//
//    val data = mutableMapOf<String, List<Int>>()
//    data["filter1"] = listOf(0, 5, 6, 2, 2, 9)
//    data["filter2"] = listOf(4, 0, 6, 1, 3, 89)
//    data["filter3"] = listOf(4, 5, 0, 2, 3, 8)

    val dataPair = mapOf(
        Pair("filter1", listOf(1, 5, 6, 2, 10, 9)),
        Pair("filter2", listOf(0, 4, 6, 88, 2, 9)),
        Pair("filter3", listOf(0, 50, 7, 2, 2, 74))
    )

    val average23 = dataPair.flatMap { it.key.indices }.average()
    println(average23)
}
