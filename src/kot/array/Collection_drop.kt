package kot.array

fun main() {

    val array = (0..10).toList()
    val employees = array.map { "Сотрудник №$it" }
    val last30 = employees.drop (3)
    for (employee in last30) {
        println(employee)
    }
}