package kot.array

fun main(){

    val array = generateSequence {
        (Math.random() * 100).toInt()

    }
    val eventList = array.take(10)
    for (i in eventList){
        println(i)
    }
}