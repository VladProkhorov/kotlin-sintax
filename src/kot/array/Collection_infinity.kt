package kot.array

fun main() {

    val array = generateSequence('A') {
        println("сгенерированно: ${it + 2}")
        it + 2
    }

    val evenList = array.take(10)
    for (i in evenList){
        println(i)}
}
