package kot.array

fun main() {
    val array = generateSequence("Сотрудник №1"){
        val index = it.substring(11).toInt()
        "Сотрудник №${index + 1}"
    }
    val infinity = array.take(100)
    for (employee in infinity ){
        println(employee)
    }

}