package kot.array

fun main(){

//    8-499-000-00-01,
//    8-499-000-00-02,
//    8-499-000-00-03,
//    8-499-000-00-04,
//    8-499-000-00-05

//    "Алина","Игорь","Елена","Сергей","Максим"

    val names = mutableListOf<String>()
    val phone = mutableListOf<Long>()
    for (i in 0..1000){
        names.add("Имя$i")
        phone.add(79_000_000_000 + (Math.random() * 1_000_000_000).toLong())
    }
    val users = names.zip(phone)
    for (user in users){
        println("Имя: ${user.first} Телефон: ${user.second}")
    }
}