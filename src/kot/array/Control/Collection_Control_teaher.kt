package kot.array.Control

fun main() {
    val data = mapOf(
        "Январь"
                to listOf(100, 100, 100, 100),
        "Февраль"
                to listOf(200, 200, -190, 200),
        "Март"
                to listOf(300, 180, 300, 100),
        "Апрель"
                to listOf(250, -250, 100, 300),
        "Май"
                to listOf(200, 100, 400, 300),
        "Июнь"
                to listOf(200, 100, 300, 300)
    )
    printInfo(data)
}

fun printInfo(data: Map<String, List<Int>>) {

    val validData = data.filterNot { it.value.any { it < 0 } }

// средняя в неделю
    val averageWeek = validData.flatMap { it.value }.average()
    println("Средняя выручка в неделю - $averageWeek")

    val listOfSum = validData.map { it.value.sum() }
//мах в месяц
    val max = listOfSum.max()
//мин в месяц
    val min = listOfSum.min()

// средняя за месяц
    val averageMonth = listOfSum.average()

    val maxMonth = validData.filter { it.value.sum() == max }.keys
    val minMonth = validData.filter { it.value.sum() == min }.keys

    print("Средняя выручка за месяц - $averageMonth")
    println("\nМаксимальная выручка в месяц - $max")
    print("Была в следующих месяцах: ")
    for (month in maxMonth) {
        print("$month ")
    }

    println("\nМинимальная выручка в месяц - $min")
    print("Была в следующих месяцах: ")
    for (month in minMonth) {
        print("$month ")
    }


    val invalidData = data.filter { it.value.any { it < 0 } }
    val errorMonth = invalidData.keys
    println("\nОшибки произошли в следующих месяцах: ")
    for (month in errorMonth) {
        print("$month ")
    }
}



