package kot.array.Control

fun main() {
    val data = mapOf(
        "Январь"
                to listOf(100, 100, 100, 100),
        "Февраль"
                to listOf(200, 200, -190, 200),
        "Март"
                to listOf(300, 180, 300, 100),
        "Апрель"
                to listOf(250, -250, 100, 300),
        "Май"
                to listOf(200, 100, 400, 300),
        "Июнь"
                to listOf(200, 100, 300, 300)
    )
// средняя в неделю
    val sum1 = mutableListOf<Int>()
    val sum2 = mutableListOf<Int>()
    val sum3 = mutableListOf<Int>()
    val sum4 = mutableListOf<Int>()
    data.forEach() {

        it.value.forEachIndexed { index, i ->
            when (index) {
                0 -> sum1 += i
                1 -> sum2 += i
                2 -> sum3 += i
                3 -> sum4 += i
                else -> throw Exception()
            }
        }
    }
    val averageWeek = listOf(sum1.average(), sum2.average(), sum3.average(), sum4.average()).joinToString()

// средняя за месяц
    val averageOfMonth = data.flatMap { it.value }.average()

//мин в месяц
    val averageOfMonthMin = data.flatMap { it.value }.min()

//мах в месяц
    val averageOfMonthMax = data.flatMap { it.value }.max()

//мах в след.месяцах
    val maxMonth = data.maxBy {
        var sum = 0
        it.value.forEach { i -> sum += i }
        sum
    }?.key

//мин в след.месяцах
    val minMonth = data.minBy {
        var sum = 0
        it.value.forEach { i -> sum += i }
        sum
    }?.key

//ошибки в месяцах
    val errorMonth = data.filter { it.value.any { it < 0 } }.keys.joinToString()


    val printInfo = data
    println("Отчет: ")
    println("Средняя выручка в неделю - $averageWeek, ")
    println("Средняя выручка за месяц - $averageOfMonth, ")
    println("Минимальная выручка  в месяц - $averageOfMonthMin,")
    println("Была в следующих месяцах: $minMonth")
    println("Максимальная выручка в месяц - $averageOfMonthMax, ")
    println("Была в следующих месяцах: $maxMonth")
    println("Ошибки произошли в следующих месяцах: $errorMonth")


}